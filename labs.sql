

create table Branch (
	bno integer not null, primary key (bno),
	street varchar(30) not null,
	city varchar(20) not null,
	tel_no integer unique,
	check (city in('vitebsk', 'brest', 'grodno'))
	);


create table Stuff(
	sno integer not null, primary key(sno),
	fname varchar2(30) not null,
	lname varchar2(30) not null,
	address varchar2(30) not null,
	tel_no integer not null,
	position varchar2(30) not null,
	sex varchar2(6) not null,
	dob date not null,
	salary integer not null,
	bno integer not null
	);

create table Property_for_rent (
	pno integer not null, primary key(pno),
	street varchar2(30) not null,
	city varchar2(30) not null,
	pref_type char not null,
	rooms integer not null,
	sno integer not null,
	bno integer not null
	);

create table Renter (
	rno integer not null, primary key (rno),
	fname varchar2(30) not null,
	lname varchar2(30) not null,
	address varchar2(30) not null,
	tel_no integer not null,
	pref_type char not null,
	max_rent integer not null
	);

create table Owner (
	ono integer not null, primary key(ono),
	fname varchar2(30) not null,
	lname varchar2(30) not null,
	address varchar2(30) not null,
	tel_no integer not null
	);

create table Viewing (
	rno integer not null,
	pno integer not null,
	date1 date not null,
	comment1 varchar2(80)
	);


alter table Stuff add constraint fk_bno_Branch foreign key (bno) references Branch;
alter table Stuff add constraint choise_sex check(sex = 'male' or sex = 'female');

alter table Property_for_rent add ono integer not null;
alter table Property_for_rent add rent integer not null;
alter table Property_for_rent add constraint choise_pref_type_pfr check(pref_type = 'h' or pref_type = 'f');

alter table Property_for_rent add constraint fk_ono_Owner foreign key (ono) references Owner;
alter table Property_for_rent add constraint fk_sno_Stuff foreign key (sno) references Stuff;
alter table Property_for_rent add constraint fk_bno_Branch_prop foreign key (bno) references Branch;


alter table Renter add  bno integer not null;
alter table Renter add constraint choise_pref_type  check(pref_type = 'h' or pref_type = 'f');
alter table Renter add constraint fk_bno_Branch_renter foreign key (bno) references Branch;


alter table Viewing add constraint pk_Renter_Property_for_rent  primary key (rno, pno);
alter table Viewing add constraint fk_rno_Renter foreign key (rno) references Renter;
alter table Viewing add constraint fk_pno_Property_for_rent foreign key (pno) references Property_for_rent;



INSERT INTO Branch VALUES (1,'Pushkina', 'brest', 4355678);
INSERT INTO Branch VALUES (2,'Kirova', 'grodno', 4355458);
INSERT INTO Branch VALUES (3,'Kolotushkina', 'grodno', 4444678);
INSERT INTO Branch VALUES (4,'Putevaya', 'brest', 4354788);
INSERT INTO Branch VALUES (5,'Hermanovskaya', 'vitebsk', 4234678);
INSERT INTO Branch VALUES (6,'Smolenskaya', 'vitebsk', 7356478);
INSERT INTO Branch VALUES (7,'Kolasa', 'brest', 7656478);
 
INSERT INTO Stuff VALUES (1, 'Misha', 'Mironov', 'Kievskaya', 2348759, 'first', 'male', TO_DATE('09.11.1978', 'DD.MM.YYYY'), 900, 	5);
INSERT INTO Stuff VALUES (2, 'Fedya', 'Alekseev', 'Karla Marksa', 2348759, 'second', 'male', TO_DATE('09.12.1978', 'DD.MM.YYYY'), 900, 6);
INSERT INTO Stuff VALUES (4, 'Vova', 'Suxanov', 'Lelyvelya', 2456759, 'first', 'male', TO_DATE('01.11.1978', 'DD.MM.YYYY'), 800, 3);
INSERT INTO Stuff VALUES (3, 'Avanysyan', 'Govnokushin', 'Revolutiaonnaja', 23487549, 'second', 'male', TO_DATE('09.12.1978', 'DD.MM.YYYY'), 900, 6);
INSERT INTO Stuff VALUES (6, 'Sasha', 'Bolshakov', 'Avtomobilistov', 9743459, 'first', 'male', TO_DATE('11.09.1980', 'DD.MM.YYYY'), 1200, 3);
INSERT INTO Stuff VALUES (5, 'Viktor', 'Psikov', 'Kolasa', 234259, 'second', 'male', TO_DATE('09.12.1978', 'DD.MM.YYYY'), 900, 6);
INSERT INTO Stuff VALUES (7, 'Lena', 'Guseva', 'Avtomobilistov', 9564359, 'second', 'female', TO_DATE('14.11.1988', 'DD.MM.YYYY'), 1000, 4);

INSERT INTO Renter VALUES (1, 'Petya', 'Komarov', 'Kosmonavtov', 4445566, 'h', 500, 6);
INSERT INTO Renter VALUES (2, 'Natasha', 'Andreeva', 'Beregovaya', 5554466, 'f', 700, 5);
INSERT INTO Renter VALUES (3, 'Andrey', 'Kudryavqev', 'Volinskaya', 4568761, 'f', 700, 4);
INSERT INTO Renter VALUES (4, 'Galya', 'Gorshkova', 'Volinskaya', 4846561, 'h', 650, 3);
INSERT INTO Renter VALUES (5, 'Vasya', 'Ermolaev', 'Boldina', 9986754, 'f', 650, 2);
INSERT INTO Renter VALUES (6, 'Olya', 'Nesterova', 'Gribnaya', 5555567, 'h', 800, 1);
INSERT INTO Renter VALUES (7, 'Nastya', 'Nesterova', 'Vielosipednaya', 55355567, 'h', 800, 7);
INSERT INTO Renter VALUES (8, 'Galina', 'Yii', 'Novaya', 55355567, 'h', 800, 7);


INSERT INTO Owner VALUES (1, 'Pasha', 'Lebedev', 'Brest ul.Gogolya', 1234765);
INSERT INTO Owner VALUES (2, 'Lesha', 'Lazarev', 'Brest ul.Zavetnaya', 1333465);
INSERT INTO Owner VALUES (3, 'Dasha', 'Simonava', 'Grodno ul.Dombrovskogo', 1555585);
INSERT INTO Owner VALUES (4, 'Masha', 'Romanova', 'Grodno ul.Zapadnaya', 7676764);
INSERT INTO Owner VALUES (5, 'Stepa', 'Melnikov', 'Vitebsk ul.Kalinina', 5454345);
INSERT INTO Owner VALUES (6, 'Sergey', 'Ponomarev', 'Vitebsk ul.Olxovaya', 8762340);

alter table Property_for_rent drop constraint fk_bno_Branch_prop;
INSERT INTO Property_for_rent VALUES (1, 'K lenovaya', 'Brest', 'h', 2,  1, 7, 4, 650);
INSERT INTO Property_for_rent VALUES (2, 'Kuybisheva', 'Brest', 'f', 4, 1, 7, 4, 1000);
INSERT INTO Property_for_rent VALUES (3, 'Elskogo', 'Grodno', 'h', 1, 4, 4, 3, 350);
INSERT INTO Property_for_rent VALUES (4, 'Novaya', 'Grodno', 'f', 3, 3, 6, 3, 650);
INSERT INTO Property_for_rent VALUES (5, 'Pionerskaya', 'Vitebsk', 'h', 3, 5, 1, 5, 650);
INSERT INTO Property_for_rent VALUES (6, 'Sportivnaya', 'Vitebsk', 'h', 5, 6, 2, 6, 1200);

INSERT INTO Viewing VALUES (1, 2, TO_DATE('12.08.2015', 'DD.MM.YYYY'), 'comment');
INSERT INTO Viewing VALUES (1, 1, TO_DATE('12.08.2015', 'DD.MM.YYYY'), '');
INSERT INTO Viewing VALUES (2, 1, TO_DATE('12.08.2015', 'DD.MM.YYYY'), '');
INSERT INTO Viewing VALUES (2, 4, TO_DATE('12.08.2015', 'DD.MM.YYYY'), '');
INSERT INTO Viewing VALUES (2, 2, TO_DATE('12.11.2015', 'DD.MM.YYYY'), 'special for view');



select * from branch;
select * from stuff;
select * from Property_for_rent;
select * from Renter;
select * from owner;
select * from Viewing;

--lab2 10variant
--орендаторы, котрый желают снять 4-х комнатные квартиры
select fname, lname from Renter
	where bno in 
		(select bno from Property_for_rent
			where rooms = 4);

--количество орендаторов, которые осмотрели квартиры и оставили комментарии
select count(rno) from Viewing
	where comment1 is not null;

--вывести информацию об офисах, в которых работают более трех сотрудников
select branch.* from Branch branch
	join (select stuff.bno
			from Stuff stuff
			group by stuff.bno
			having count(1) >= 3) stuff
	on stuff.bno = branch.bno;

--вывести информацию о сотрудниках того отделения в которм не обслуживается ни один объект недвижимости
select * from Stuff stuff
	left join Property_for_rent pfr
	on stuff.bno = pfr.bno
	where pfr.bno is null;


--lab3 
--информация об офисах в бресте
create or replace view brest_branches as
	select * from Branch
		where city in 'brest';
select * from brest_branches;

--информация об объектах недвижимости минимальной стоимости
create or replace view minimum_pfr_rent as
	select * from property_for_rent
	where rent = (select (min(rent)) from property_for_rent);
select * from minimum_pfr_rent;

--информация о количестве сделаных просмотров с комментариями
create or replace view comment_views as
	select (count(comment1)) as viewing_with_comments from Viewing
	where comment1 is not null;
select * from comment_views;

--сведения об арендаторах, желающих орендовать 3-х комнатные квартиры
	-- в тех же городах, что они и живут
create or replace view renter_same_city_apart as
	select r.* from Renter r
		where r.address = 
			(select pfr.street from Property_for_rent pfr
				where (r.adress like '%'|| pfr.street || '%'));
select * from renter_same_city_apart;

--отделения с максимальным количеством работающих сотрудников
create or replace view max_employees_branch as
	select * from Branch
		where bno in(
			select bno from Stuff
			group by bno
			having count(bno) in
				(select max(count(sno)) from Stuff
					group by bno));
select * from max_employees_branch;

--информация о сотрудниках и объекта, котрые они предлагают в аренду в текущем квартале
create or replace view info_current_quartal as
	select stuff.* from Stuff stuff join Property_for_rent pfr
		on stuff.sno = pfr.sno
			where pno in 
				(select pno from Viewing
					where to_char(date1,'Q')=to_char(sysdate, 'Q'));
select * from info_current_quartal;

-- информация о владельцах, чьи дома просматривались орендаторами более двух раз
create or replace view watched_morethan2times as
	select * from Owner
	where ono in (
		select ono from Property_for_rent
		where pno in(
				select pno from Viewing
				group by pno
				having count(rno)>=2 ));

select * from watched_morethan2times;