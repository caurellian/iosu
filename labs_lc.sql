create table Details (
	detail_id integer not null, primary key(detail_id),
	name varchar(50),
	gost varchar(50),
	material_id integer not null,
	shape_id integer not null,
	meterial_amount integer not null,
	workCost integer not null
);

create table Material (
	material_id integer not null, primary key(material_id),
	name varchar(50),
	stockResource integer not null,
	costforone integer not null
);


create table Shapes (
	shape_id integer not null, primary key(shape_id),
	size_id integer
);

create table Sizes (
	size_id integer not null, primary key(size_id),
	type varchar(50) not null
);

alter table Orders modify( sellDate null);
create table Orders (
	order_id integer not null, primary key(order_id),
	client_id integer not null,
	detail_id integer not null,
	amount integer not null,
	orderDate date not null,
	sellDate date,
	cost integer not null
);

--drop client and replace with that
create table Clients (
	client_id integer not null, primary key(client_id),
	fname varchar(50),
	lname varchar(50),
	llname varchar(50),
	adress_id integer
);

create table Countries (
	country_id integer not null, primary key(country_id),
	name varchar(50)
);

create table Cities (
	city_id integer not null, primary key(city_id),
	name varchar(50)
);

create table CountriesAndCities (
	adressID integer not null,
	country_id integer not null,
	city_id integer not null
);


--Details prop
alter table Details add constraint fk_materialID_Material foreign key(material_id) references Material;
alter table Details add constraint fk_shapeId_Shape foreign key(shape_id) references Shapes;
alter table Details add material_amount integer;
alter table Details enable constraint fk_materialID_Material;
alter table Details enable constraint fk_shapeId_Shape;

--shapes props
alter table Shapes add constraint fk_sizeId_Size foreign key(size_id) references Sizes;


--sizes props
alter table Sizes add constraint size_choise check(type = 'huge' or type = 'large' or type = 'medium');


--orders props
alter table Orders add constraint fk_clientID_Client foreign key(client_id) references Clients;
alter table Orders add constraint fk_detailID_Detail foreign key(detail_id) references Details;

--countryAndCityProps
alter table CountriesAndCities add constraint fk_countryID_Country foreign key(country_id) references Countries;
alter table CountriesAndCities add constraint fk_cityID_City foreign key(city_id) references Cities;
alter table CountriesAndCities add constraint pk_CaC primary key(adressID);

--client props
alter table Clients add constraint fk_adressID_CountryAndCity foreign key(adress_id) references CountriesAndCities;
alter table Clients
 modify (
	 adress_id integer
);
alter table Clients rename column fname to fname_tmp;
alter table Clients rename column lname to fname;
alter table Clients rename column fname_tmp to lname;



--sequences
create sequence DetailsSequence
	increment by 1
	start with 1;
create sequence MaterialSequence
	increment by 1
	start with 1;
create sequence ShapesSequence
	increment by 1
	start with 1;
create sequence SizesSequence
	increment by 1
	start with 1;
create sequence OrdersSequence
	increment by 1
	start with 1;
create sequence ClientlSequence
	increment by 1
	start with 1;
create sequence CountriesAndCitiesSequence
	increment by 1
	start with 1;
create sequence CountriesSequence
	increment by 1
	start with 1;
create sequence CitiesSequence
	increment by 1
	start with 1;

--table fill
	--size
	insert into Sizes
		values(SizesSequence.nextval, 'medium');
	insert into Sizes
		values(SizesSequence.nextval, 'large');
	insert into Sizes
		values(SizesSequence.nextval, 'huge');

	--shapes
	insert into Shapes
		values(ShapesSequence.nextval, '3');
	insert into Shapes	
		values(ShapesSequence.nextval, '2');
	insert into Shapes
		values(ShapesSequence.nextval, '3');
	insert into Shapes
		values(ShapesSequence.nextval, '2');
	insert into Shapes	
		values(ShapesSequence.nextval, '2');
	insert into Shapes	
		values(ShapesSequence.nextval, '3');
	insert into Shapes	
		values(ShapesSequence.nextval, '2');

	--Materials
	insert into Material
		values(MaterialSequence.nextval, 'Stal', '500','50');
	insert into Material
		values(MaterialSequence.nextval, 'Zoloto', '500','34');
	insert into Material
		values(MaterialSequence.nextval, 'Chugun', '500','25');
	insert into Material
		values(MaterialSequence.nextval, 'Latun', '500','64');
	insert into Material
		values(MaterialSequence.nextval, null, '500', '43');
	insert into Material
		values(MaterialSequence.nextval, 'Lom', '500', '74');


	--Details
	insert into Details
		values(DetailsSequence.nextval, 'Gaika', '1478', '1', '1', '13',,'70');
	insert into Details
		values(DetailsSequence.nextval, 'Doloto', '1489', '2', '2','20','30');
	insert into Details
		values(DetailsSequence.nextval, 'Bolt', '1488', '3', '3','21','60');
	insert into Details
		values(DetailsSequence.nextval, 'Stameska', '1488', '1', '4','24','10');
	insert into Details
		values(DetailsSequence.nextval, 'Zvezda velosipeda', '1488', '4', '5','26','70');
	insert into Details
		values(DetailsSequence.nextval, 'Rama gruzovika ', '1488', '2', '6','24','85');
	insert into Details
		values(DetailsSequence.nextval, 'Toporische', '1488', '3', '7','23','20');
	insert into Details
		values(DetailsSequence.nextval, 'Gaika', '1488', '4', '2','22','35');
	insert into Details
		values(DetailsSequence.nextval, 'Gaika', '1488', '5', '2', '200','40');


	update Details
		set meterial_amount = 1
		where detail_id=5;


	--Countries
	insert into Countries
		values(CountriesSequence.nextval, 'Belarus');
	insert into Countries
		values(CountriesSequence.nextval, 'Russia');
	insert into Countries
		values(CountriesSequence.nextval, 'Kambodja');
	insert into Countries
		values(CountriesSequence.nextval, 'Ukraine');

	--Cities
	insert into Cities
		values(CitiesSequence.nextval, 'Minsk');
	insert into Cities
		values(CitiesSequence.nextval, 'Voronegh');
	insert into Cities
		values(CitiesSequence.nextval, 'Kampot');
	insert into Cities
		values(CitiesSequence.nextval, 'Kiev');
	insert into Cities
		values(CitiesSequence.nextval, 'Sankt-Peterburg');
	insert into Cities
		values(CitiesSequence.nextval, 'Stolin');

	--Countries and Cities
	insert into CountriesAndCities
		values(CountriesAndCitiesSequence.nextval, '1', '1');
	insert into CountriesAndCities
		values(CountriesAndCitiesSequence.nextval, '1', '6');
	insert into CountriesAndCities
		values(CountriesAndCitiesSequence.nextval, '2', '2');
	insert into CountriesAndCities
		values(CountriesAndCitiesSequence.nextval, '3', '3');
	insert into CountriesAndCities
		values(CountriesAndCitiesSequence.nextval, '4', '4');
	insert into CountriesAndCities
		values(CountriesAndCitiesSequence.nextval, '2', '5');


	--clients
	insert into Clients
		values(ClientlSequence.nextval, 'Petrov', 'Petr', 'Petrovich', '1');
	insert into Clients
		values(ClientlSequence.nextval, 'Ivanov', 'Ivan', 'Ivanovich', '2');
	insert into Clients
		values(ClientlSequence.nextval, 'Bobryk', 'Bobr', 'Zhivotnovich', '3');
	insert into Clients
		values(ClientlSequence.nextval, 'Lokhovich', 'Vasya', 'IV', '4');
	insert into Clients
		values(ClientlSequence.nextval, 'Vyterayko', 'Oleg', 'Vyacheslavovich', '5');
	insert into Clients
		values(ClientlSequence.nextval, 'Gothic', 'Ignat', 'Victorovich', '6');
	insert into Clients
		values(ClientlSequence.nextval, 'Petrova', 'ALLa', 'Genadyevna', '6');

	--orders
	alter session set nls_date_format='MM.DD.YYYY';
	insert into Orders
		values(OrdersSequence.nextval, '2', '2', '30', '01.22.2007', '01.22.2007', '6300000');
	alter session set nls_date_format='MM.DD.YYYY';
	insert into Orders
		values(OrdersSequence.nextval, '3', '3', '120', '02.20.2004', '05.20.2004', '40000');
	alter session set nls_date_format='DD.MM.YYYY';
	insert into Orders
		values(OrdersSequence.nextval, '2', '1', '120', '20.02.2004', null, '40000');
	alter session set nls_date_format='DD.MM.YYYY';
	insert into Orders
		values(OrdersSequence.nextval, '3', '2', '120', '12.12.2004', null, '40000');



	--synonym
	create synonym CaC for CountriesAndCities;


-----------------lab2--------------------------------------------
--------------------task 2---------------------------------------------

---condition req(biggest orders)
select client_id, detail_id, amount
	from Orders
	where amount > 150;

--total req(counts order amount for client)
select client_id, count(detail_id)
	from Orders
	group by client_id;



--param req(details that selled in some date interval) -- Working without declare
declare
	startdate date;
begin
 	select '24-nov-2015' into startdate from dual;
end;
/

declare
	enddate date;
begin
 	select '24-nov-2014' into enddate from dual;
end;
/

select detail_id, client_id
	from Orders
	where detail_id in 
		(select detail_id
			from Orders
			where sellDate >= '&startdate' and sellDate <= '&enddate');

--union req(Details with material)
select name as "Details and material" from Details
	union
	select  name from Material;

--date manipulation req(selled item within quartals)
select  to_char(sellDate, 'Q') as "Year quartal", sum(cost) as "SUM"
	from Orders
	inner join Details on Details.detail_id = Orders.detail_id
	group by to_char(Orders.sellDate, 'Q');

-----------------------task 3-----------------------
-- inner join req(Details and materials)
select D.name, M.name from Details D
	inner join Material M on D.material_id = M.material_id
	group by(D.name, M.name);

--full join
select D.name, M.name from Details D
	right join Material M on D.material_id = M.material_id;

--in req(details from gold gold)
select D.name, M.name from Details D
	left join Material M on D.material_id = M.material_id
	where M.material_id in
		(
			select material_id from Material
			where name='Zoloto' or name='Stal');

--any req(returns Details from material which request amount of material less
	-- then any detail from gold)
select * from Details
where material_amount < any (
	select max(material_amount) from Details where(material_id = 2) group by name)
	order by material_amount;

--exists req(ordered but not selled)
select * from Clients
	where exists
		(select client_id from Orders
			where sellDate is null and Clients.client_id = Orders.client_id);


--lab3
--горизонтальное обновляемое представление
create or replace view orders_on_currentq as
	select * from Orders
		where to_char(orderDate,'Q')=to_char(sysdate, 'Q')
		with check option;
		--current quartal
		insert into orders_on_currentq 
			values(OrdersSequence.nextval, '5', '4', '500', '13-nov-2015', '26-dec-2015', '50000');
		--not curent cuartal
		insert into orders_on_currentq 
			values(OrdersSequence.nextval, '5', '4', '500', '12-jul-2015', '22-jul-2015', '50000');
select * from orders_on_currentq;


--вертикальное необновляемое
create or replace view order_stat as
	select order_id, detail_id, amount, orderdate, sellDate from Orders;
select * from order_stat;


--------------------------------------------------------------------------------------------------------
--lab4

--хранимая процедура, которая выводит количество деталей заданного наименования.(дополнение
	--и если нет заказов на эту деталь нет в течение года, то удаляет его из исходной таблицы)
		--использовать dbms_output для отладки программы
--вывести среднюю цену заказов по гордам
--исключения
--------------------------------------------------------------------------------------
select Details.name, sum(Orders.amount) from Orders
	inner join Details on Details.detail_id = Orders.detail_id
	where ((sysdate  - Orders.selldate) > 365)
	group by Details.name;

--------------------------------------------------------------------------------------

set serveroutput on;
create or replace package lab4Pack as
	procedure countDetailsByName(detailName varchar);
	procedure updateSellDateByOrderId(orderId integer);
	procedure deleteDetailsWithoutDemand(detailName varchar);
	procedure deleteDecision(decision varchar, detailName varchar);
	function getAvgOrderPriceByCountry return varchar2;
end;
/

create or replace package body lab4Pack is
	procedure countDetailsByName(detailName varchar) as
		cursor countDetailsCursor is
			select count(detail_id) from Details
			where (name = detailName);

		detailAmount integer;
		i number(10);
		n number(10);
		nullPointerException exception;


		BEGIN
			sys.dbms_output.enable;
		    open countDetailsCursor;
		    fetch countDetailsCursor into n;
				if (n=0) then raise nullPointerException;
				else
					sys.dbms_output.put_line(detailName||' '||n);
				END IF;
		     close countDetailsCursor;

		    exception
		    when nullPointerException then
		     	raise_application_error(-20001, 'no details with name like this');
  	end countDetailsByName;
    
  procedure deleteDetailsWithoutDemand(detailName varchar) as
		cursor selectDetailsWithYearExpired(detailName varchar) is
			select Details.detail_id, sum(Orders.amount) as orderSum from Orders
			inner join Details on Details.detail_id = Orders.detail_id
			where Details.name = detailName and ((sysdate  - Orders.selldate) > 365)
			group by Details.detail_id;

		nullPointerException exception;
    childRecordException exception;
		n number(10);
    PRAGMA EXCEPTION_INIT(childRecordException, -2292);
		
		BEGIN
			sys.dbms_output.enable;
		    for i in selectDetailsWithYearExpired(detailName) loop
          dbms_output.put_line(i.detail_id||' '||i.orderSum);
          delete from Details where Details.detail_id = i.detail_id;         
		    end loop;
        
		    exception
		    when nullPointerException then
		     	raise_application_error(-20001, 'no details with name like this');
        when childRecordException then
          begin
            deleteDecision('y', detailName);
          end;
          when others then
          raise_application_error(-20002, 'Unexpected error occured');
  	end;
	/  
    
  procedure deleteDecision(decision varchar, detailName varchar) as
    
      wrongValueException exception;
      
      cursor selectDetailsWithYearExpired(detailName varchar) is
        select Details.detail_id, sum(Orders.amount) as orderSum from Orders
        inner join Details on Details.detail_id = Orders.detail_id
        where Details.name = detailName and ((sysdate  - Orders.selldate) > 365)
        group by Details.detail_id;


      begin     
        if (decision = 'n') then
          dbms_output.put_line('U make a good decision');
        elsif (decision = 'y') then
          for i in selectDetailsWithYearExpired(detailName) loop
            delete from Orders where Orders.detail_id = i.detail_id;
            delete from Details where Details.detail_id = i.detail_id;  
          end loop;
          dbms_output.put_line('Details without demand were dropped');            
        else 
            raise wrongValueException;
        end if;
        
        exception
          when wrongValueException then
            dbms_output.put_line('Uncorrect parameter');
      end;
  	
    
    
  function getAvgOrderPriceByCountry
    return varchar2 is outState varchar2(500);

	  	cursor getAvgCostByCountryCursor is
	  		select Countries.name as Countries_name, avg(Orders.cost) as avgCost from Orders
	        inner join Clients on Orders.client_id = Clients.client_id
	        inner join CountriesAndCities on Clients.adress_id = CountriesAndCities.adressid
	        inner join Countries on CountriesAndCities.country_id = Countries.country_id
        group by Countries.name;

		begin
			for i in getAvgCostByCountryCursor loop
			 	outState := outState || chr(13) || i.Countries_name ||' '|| i.avgCost;
			end loop;
			return outState;
	end getAvgOrderPriceByCountry; 				
end;
/

BEGIN
  deleteDetailsWithoutDemand('Doloto');
END;
/ 
+--второй вид ошибок
+--одна процедура из правильных

--------------------------------------------lab5------------------------
----------------------------------------триггеры------------------------
--три бизнес логики(Листик)
	--+при новом заказе смотерть можно ли сделать столько деталей исходя из имеющегося материала
	--+при новом заказе смотреть количество и решать может ли столько деталей выплавить цех на этот год
	--+при новом заказе автоматически заполнять поле с ценой заказа
--+системный и +ддл
-------------------------------------------------------------------------

-----------------------------компаунд на бизнес-логику-------------------
-----1)При новом заказе высчитывать количество материала, которое придется затратить на него
-----и сверять с тем, какое количество материала уже есть на складе.
-----Если такого количества нет на складе, то взять заказ на столько деталей, на сколько хватит материала на складе.
-----2)также проверяет может ли предприятие выплавить такое количество деталей исходя из нормы нагрузки в год
-----3)автоматически назначает полю cost вычисленное значение с учетом цены одной детали, количества и цены материала за единицу

create table ManufactoryLoad (
		year varchar2(4) not null, primary key(year),
		load integer not null
	);

set serveroutput on;

create or replace trigger checkMaterialAmountTrigger
    for insert or update on Orders
    referencing old as old new as new
  compound trigger

    vStockRecource integer;
    vDetailMaterialAmount  integer;
    usedMaterialId integer;
    materialCost integer;
    detailWorkCost integer;

	yearManufactoryLoad integer := 800;
	lastYear varchar2(5);
	currentManufactoryLoad integer;
    noMoreMaterialOnStockException exception;


    before each row is
      BEGIN
        dbms_output.enable;
        -- check material on stock
        select stockResource into vStockRecource from Material
         	 where material_id = (
           		 select material_id from Details
              		where detail_id = :new.detail_id
        );

        -- set variable for material amount of each detail
        select material_amount into vDetailMaterialAmount from Details
          where detail_id = :new.detail_id;

        -- set variable of used matedial to avoid mutating tables
        select material_id into usedMaterialId from Details
            	where detail_id = :new.detail_Id;



		--set variable for every peace of material cost
		select costforone into materialCost from Material
			inner join Details on Details.material_id = Material.material_id
			where Details.detail_id = :new.detail_id;

		-- set variable for last year
       	select max(year) into lastYear from ManufactoryLoad;

       	-- set variable for current ManufactoryLoad
		select load into currentManufactoryLoad from ManufactoryLoad
			where year = to_char(:new.orderDate, 'YYYY');

		--set variable for detail work cost
		select workCost into detailWorkCost from Details
			where detail_id = :new.detail_id;



        -- calculate new amount of order if material amount is less then on stock
        if ( (vDetailMaterialAmount * :new.amount) > vStockRecource and  vStockRecource >= 0) THEN
          :new.amount := floor(vStockRecource / vDetailMaterialAmount);
	        dbms_output.put_line('On stock we have material only for ' || :new.amount);
	        if (:new.amount > (yearManufactoryLoad - currentManufactoryLoad)) then
            :new.amount := yearManufactoryLoad - currentManufactoryLoad;
            dbms_output.put_line('But manufactory load is to high. Only ' || :new.amount || ' will be proceed due ManufactoryLoad');
	       	end if;
        else
          dbms_output.put_line(:new.amount || ' of details where sold');
        end if;

        if (:new.amount > (yearManufactoryLoad - currentManufactoryLoad)) then
		   		:new.amount := yearManufactoryLoad - currentManufactoryLoad;
          dbms_output.put_line('But manufactory load is to high. Only ' || :new.amount || ' will be proceed due ManufactoryLoad');
        end if;

        --automaticaly calculate price
        :new.cost := ((materialCost * vDetailMaterialAmount * :new.amount) +  (detailWorkCost * :new.amount));



	exception
		when noMoreMaterialOnStockException then
		raise_application_error(-20101, 'No such material amount to proceed your order');
			rollback;
    END before each row;

    --update material on stock
    after each row is
	    begin
	      	update Material
		        set stockResource = stockResource - :new.amount * vDetailMaterialAmount
		        where material_id = usedMaterialId;

	     	update ManufactoryLoad
				set load = load + :new.amount
				where year = to_char(:new.orderDate, 'YYYY');
    end after each row;

    after statement is
	    begin
		    delete from Orders
		    	where amount = 0;
    end after statement;

end checkMaterialAmountTrigger;
/

BEGIN
  insert into ORDERS
  (ORDER_ID,CLIENT_ID,DETAIL_ID,AMOUNT,ORDERDATE,SELLDATE, COST)
		values(OrdersSequence.nextval, '6', '5', '50', '25-dec-2015', '31-jan-2015', '40000'); 
END;
/
-------------------------------------------------------------------------

----------------------------for each row---------------------------------
----------------------не дает возможность добавить на склад больше-------
-------------------------5000 едениц  материалов ------------------------
create or replace trigger restrictStockOverflow
	before insert or update on Material
for each row

	begin
	    if :new.stockResource > 5000 then
			:new.stockResource := 5000;
			dbms_output.enable;
			dbms_output.put_line('Space restrict for stock!');
	    end if;
end restrictStockOverflow;
/


-----------------------------системный-----------------------------------
-------------------лог при входе пользователя в таблицу логин'ов---------
create table LoginTable(
	logdate varchar(20),
	logText varchar(50));

create or replace trigger loginTrigger
	after logon on schema

	declare
		loginDate varchar(20);
	begin
		select to_char(sysdate, 'DD-MM-YY HH24:MI:SS') into loginDate from dual;
		
		dbms_output.enable;
			insert into LoginTable values(loginDate, 'User ' ||user||' logged in');
end loginTrigger;
/

--------------------------DDL----------------------------------------------
------------------заносит в лог пользователя и дату исполнения------------
-----------------create, drop, alter, update комманд--------------------

create table LogTable(
	logdate varchar(20),
	logText varchar(50));

create or replace trigger ddlTrigger
	after create or alter or drop on schema
	declare
		logdate varchar(20);
	begin
		select to_char(sysdate, 'DD-MM-YY HH24:MI:SS') into logDate from dual;
		dbms_output.enable;
		insert into LogTable
			values(logdate, 'User ' ||user||' did somtehing ddl with this DB');
end ddlTrigger;
/

------------------------------------------------------------------------------


-------------------------------------lab 6--------------------------------------------------------
---------------------------------сложный поиск----------------------------------------------------
--------поиск имени клиентов и названия деталей исходя из параметров заказа-----------------------
create or replace procedure searchOrderBuyers(order_id_Filter in number default null,
											amount_Filter in number default null,
											orderDate_Filter in varchar2 default null,
											sellDate_Filter in varchar2 default null,
											cost_Filter in number default null) is


	sqlQuery dbms_sql.varchar2s;
	whereString dbms_sql.varchar2s;
	i integer;
	n integer;
	cursorIdentifier integer;
	clientLNameSelected Clients.lname%type;
	clientFNameSelected Clients.fname%type;
	detailNameSelected Details.name%type;
	whereWord varchar2(50);

	result number;



BEGIN
	whereString(1) := 'where ';
	dbms_output.enable;

	--make where string for dynamic request
	if order_id_Filter is not null THEN
		whereString(whereString.last + 1) := 'o.order_id like :order_id_Filter';
	end if;

	if amount_Filter is not null THEN
	    if whereString.count > 1 then
	        whereString(whereString.last + 1) := ' and ';
	    end if;
		whereString(whereString.last + 1) := 'o.amount like :amount_Filter';
	end if;

	if orderDate_Filter is not null THEN
	    if whereString.count > 1 then
	        whereString(whereString.last + 1) := ' and ';
	    end if;
		whereString(whereString.last + 1) := 'to_char(o.orderDate) like :orderDate_Filter';
	end if;

	if sellDate_Filter is not null THEN
	    if whereString.count > 1 then
	        whereString(whereString.last + 1) := ' and ';
	    end if;
		whereString(whereString.last + 1) := 'to_char(o.sellDate) like :sellDate_Filter';
	end if;

	if cost_Filter is not null THEN
	    if whereString.count > 1 then
	        whereString(whereString.last + 1) := ' and ';
	    end if;
		whereString(whereString.last + 1) := 'o.cost like :cost_Filter';
	end if;

	sqlQuery(1) := 'select c.lname, c.fname, d.name ';
	sqlQuery(2) := '	from Clients c ';
	sqlQuery(3) := '	inner join Orders o on c.client_id = o.client_id ';
	sqlQuery(4) := '	inner join Details d on o.detail_id = d.detail_id ';


	--end up with sqlQuery adding where and option to every string
	IF whereString.count > 1
	THEN
		for i in whereString.first..whereString.last LOOP
			sqlQuery(sqlQuery.last + 1) := whereString(i);
			whereWord := ' and ';
		END LOOP;		
	END IF;

	--define the cursor
	cursorIdentifier:=dbms_sql.open_cursor;

	--parsing query text
	dbms_sql.parse(cursorIdentifier, sqlQuery, sqlQuery.first, sqlQuery.last, false, dbms_sql.native);

	--set query parameters
	if order_id_Filter is not null THEN
		dbms_sql.bind_variable(cursorIdentifier,':order_id_Filter',order_id_Filter);
	end if;

	if amount_Filter is not null THEN
		dbms_sql.bind_variable(cursorIdentifier,':amount_Filter',amount_Filter);
	end if;

	if orderDate_Filter is not null THEN
		dbms_sql.bind_variable(cursorIdentifier,':orderDate_Filter',orderDate_Filter);
	end if;

	if sellDate_Filter is not null THEN
		dbms_sql.bind_variable(cursorIdentifier,':sellDate_Filter',sellDate_Filter);
	end if;

	if cost_Filter is not null THEN
		dbms_sql.bind_variable(cursorIdentifier,':cost_Filter',cost_Filter);
	end if;


	--set up query rows
	dbms_sql.define_column(cursorIdentifier, 1, clientLNameSelected, 50);
	dbms_sql.define_column(cursorIdentifier, 2, clientFNameSelected, 50);
	dbms_sql.define_column(cursorIdentifier, 3, detailNameSelected, 50);

	--run created sqlQuery
	result := dbms_sql.execute(cursorIdentifier);

	--extract data from cursor
	LOOP
		IF dbms_sql.fetch_rows(cursorIdentifier) > 0
			THEN
				dbms_sql.column_value(cursorIdentifier, 1, clientLNameSelected);
				dbms_sql.column_value(cursorIdentifier, 2, clientFNameSelected);
				dbms_sql.column_value(cursorIdentifier, 3, detailNameSelected);
				dbms_output.put_line(clientFNameSelected ||' '|| clientLNameSelected ||' '|| detailNameSelected);
			ELSE exit;
		END IF;
	END LOOP;
END;
/


set serveroutput on;
execute searchOrderBuyers(24);

--------------------------------------------DDL-------------------------------------------------------------------
-------------------------создает столбец в таблице материлов и заносит-------------------------------------------
-------------------------сколько раз используется этот материал в деталях----------------------------------------
create or replace procedure materialUseCount(newColumn varchar) as

	alterString varchar(200);
	cursorIdentifier number;
	updateString varchar(150);
	nullArgumentsException exception;

	exec number;
begin
	dbms_output.enable;

	if newColumn is null then
		raise nullArgumentsException;
	end if;

	--adding new column to Cities
	alterString := 'alter table Material add ' ||newColumn|| ' number';
	execute immediate alterString;

	--fill up colimn
	cursorIdentifier := dbms_sql.open_cursor;
	updateString := 'update Material material set material.' ||newColumn||'=(
		select count(DETAIL_ID) from Details details
			where material.material_id = details.material_id)';
	dbms_sql.parse(cursorIdentifier, updateString, dbms_sql.native);
	exec := dbms_sql.execute(cursorIdentifier);
	dbms_sql.close_cursor(cursorIdentifier);


	exception
		when nullArgumentsException then
			raise_application_error(-20005, 'empty arguments');
end;
-------------------------------------------------------------------------------------------------------------------