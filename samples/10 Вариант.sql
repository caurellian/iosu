//Таблички
create table teams
(
  id_team number(10) primary key,
  t_name varchar(20) not null,
  shift varchar(5) not null constraint shift_c check(shift in ('1','2'))
);

create table clients
(
  id_client number(10) primary key,
  l_name varchar(20) not null,
  f_name varchar(20) not null,
  address varchar(20) not null,
  phone varchar(10) not null constraint phone_u unique
);

create table transports
(
  id_transport number(10) primary key,
  t_name varchar2(20) not null,
  exp_years number(10) not null
);
 
 create table firms
(
  id_firm number(10) primary key,
  f_name varchar2(20) not null
);

create table types
(
  id_type number(10) primary key,
  t_name varchar(20) not null
);
  
  create table products
(
  id_product number(10) primary key,
  id_firm number(10) not null,
  id_type number(10) not null,
  p_name varchar(20)  not null,
  cost number(10) not null,
  amount number (10) not null,
  
  constraint id_firm_c foreign key(id_firm) references firms(id_firm),
  constraint id_type_c foreign key(id_type) references types(id_type)
);
  
  
create table orders
(
  id_order number(10) primary key,
  id_client number(10) not null,
  id_product number(10) not null,
  id_team number(10) not null,
  id_transport number(10) not null,
  o_amount number(10) not null,
  o_date date not null,
  s_date date,
  
  constraint id_client_c foreign key(id_client) references clients(id_client),
  constraint id_product_c foreign key(id_product) references products(id_product),
  constraint id_team_c foreign key(id_team) references teams(id_team),
  constraint id_transport_c foreign key(id_transport) references transports(id_transport)
);


//последовательности
CREATE SEQUENCE seqTeams
INCREMENT BY 1
START WITH 1;

CREATE SEQUENCE seqClients
INCREMENT BY 1
START WITH 1;

CREATE SEQUENCE seqTransports
INCREMENT BY 1
START WITH 1;

CREATE SEQUENCE seqFirms
INCREMENT BY 1
START WITH 1;

CREATE SEQUENCE seqTypes
INCREMENT BY 1
START WITH 1;

CREATE SEQUENCE seqProducts
INCREMENT BY 1
START WITH 1;
 
CREATE SEQUENCE seqOrders
INCREMENT BY 1
START WITH 1;

//заполнение таблиц
insert into teams
values (seqTeams.NEXTVAL,'Vasiltsa','1');
insert into teams
values (seqTeams.NEXTVAL,'Burova','2');
insert into teams
values (seqTeams.NEXTVAL,'Veselova','1');
insert into teams
values (seqTeams.NEXTVAL,'Titova','2');

insert into clients
values (seqClients.NEXTVAL,'Sidorov','Maksim','Minsk','9874536');
insert into clients
values (seqClients.NEXTVAL,'Rodin','Aleksei','Moskva','8745634');
insert into clients
values (seqClients.NEXTVAL,'Gusev','Vasiliy','Brest','4574156');
insert into clients
values (seqClients.NEXTVAL,'Zaxarov','Aleksandr','Bryansk','9274668');

insert into transports
values (seqTransports.NEXTVAL,'MoveIT',15);
insert into transports
values (seqTransports.NEXTVAL,'FedEX',20);
insert into transports
values (seqTransports.NEXTVAL,'Transfer',4);
insert into transports
values (seqTransports.NEXTVAL,'GLG',14);

insert into firms
values (seqFirms.NEXTVAL, 'Philips');
insert into firms
values (seqFirms.NEXTVAL, 'Atlant');
insert into firms
values (seqFirms.NEXTVAL, 'LG');
insert into firms
values (seqFirms.NEXTVAL, 'Samsung');
insert into firms
values (seqFirms.NEXTVAL, 'Bosh');
insert into firms
values (seqFirms.NEXTVAL, 'Asus');

insert into types
values (seqTypes.NEXTVAL, 'vacuum cleaner');
insert into types
values (seqTypes.NEXTVAL, 'refrigerator');
insert into types
values (seqTypes.NEXTVAL, 'TV');
insert into types
values (seqTypes.NEXTVAL, 'washing machine');
insert into types
values (seqTypes.NEXTVAL, 'coffee maker');
insert into types
values (seqTypes.NEXTVAL, 'electric kettle');
insert into types
values (seqTypes.NEXTVAL, 'mediaplayer');

insert into products
values (seqProducts.NEXTVAL, 1, 5, 'HD7762', 150, 220);
insert into products
values (seqProducts.NEXTVAL, 2, 2, 'XM-4008-022', 300, 70);
insert into products
values (seqProducts.NEXTVAL, 3, 1, 'V-K69164N', 50, 285);
insert into products
values (seqProducts.NEXTVAL, 4, 3, 'UE40F6500', 800, 200);
insert into products
values (seqProducts.NEXTVAL, 5, 6, 'TWK7601', 150, 350);
insert into products
values (seqProducts.NEXTVAL, 6, 7, 'BDS-700', 180, 350); 


insert into orders
values (seqOrders.NEXTVAL, 3, 4, 4, 4, 50,'10.12.2014','12.12.2014');
insert into orders
values (seqOrders.NEXTVAL, 3, 2, 3, 3, 20,'10.15.2014','10.18.2014');
insert into orders
values (seqOrders.NEXTVAL, 4, 1, 2, 4, 150,'10.20.2014', '10.27.2014');
insert into orders
values (seqOrders.NEXTVAL, 1, 6, 4, 3, 200,'11.05.2014','11.10.2014');
insert into orders
values (seqOrders.NEXTVAL, 1, 3, 1, 1, 150,'11.13.2014',null);
insert into orders
values (seqOrders.NEXTVAL, 2, 3, 3, 2, 70,'11.14.2014',null);

//Запросы из приложения 2

//крупнейшие партии(условная выборка)
select id_client,id_product,o_amount
from orders
where o_amount > 100;

//сводка по технике клиентов(итоговый)
select id_client, count(id_product) as "Count"
from orders
group by id_client;

//Продукция, реализованная в заданный период времени(параметрический)
select id_product,p_name
from products
where id_product in (select id_product
					from orders
					where o_date >= :var1 and o_date <= :var2);
					

//Общий список клиентов и производителей(общий)
select l_name as "clients&firms" from clients
UNION
select f_name from firms;

//Количество проданных товаров фирм производителей по кварталам(запрос по полю с типом дата)
select f_name, to_char(o_date,'Q') Kvartal, sum(o_amount)
from orders
join products
on (orders.id_product=products.id_product)
join firms
on (products.id_firm=firms.id_firm)
group by f_name, to_char(o_date,'Q');

//задание по 1-й лабе
create index clients_index
on orders(id_client);

create index firms_index
on products(id_firm);

create synonym carriers
for transports;

//задание по 2-й лабе
//внутреннее соединение
select * from products join types
using (id_type);
//внешнее соединение
select * from products full join types
using (id_type);
//подзапрос с предикатом IN
select * from products
where id_firm in (select id_firm from firms
                  where f_name='Philips' or f_name='LG'
                 );

//подзапрос с предикатом ANY
//выводит телевизоры, которые стоят дешевле, чем (любой) самый дорогой телевизор фирмы Phillips				   
SELECT *
FROM products
WHERE cost < ANY (SELECT MAX(cost) FROM products WHERE (id_type = 3) and id_firm=1
group by p_name)
and id_type=3
order by cost;

//подзапрос с предикатом ALL
//выводит телевизоры, которые стоят дешевле, чем все самые дорогие телевизоры фирмы Phillips				   
SELECT *
FROM products
WHERE cost < ALL (SELECT MAX(cost) FROM products WHERE (id_type = 3) and id_firm=1
group by p_name)
and id_type=3
order by cost;
				   
//подзапрос с предикатом EXISTS
//клиенты которым не доставили какой-либо товар
select * from clients
where exists (select id_client from orders
              where s_date IS NULL and clients.id_client=orders.id_client
             );
//подзапрос с предикатом NOT EXISTS
//клиенты которым доставили весь товар				   
select * from clients
where not exists (select id_client from orders
              where s_date IS NULL and clients.id_client=orders.id_client
             );
//3 лаба
//вертикальное представление(необновляемое)
create or replace view prod as
select p_name, cost, amount
from products;
//горизонтальное представление
create or replace view phil as
select * from products
where id_firm in (select id_firm from firms
                  where f_name='Philips'
                 );
//вертикальное обновляемое представление c WITH CHECK OPTION
create or replace view tr_1 as
select *
from orders
where s_date is null
with check option;
//необновляемое представление
create or replace view tr_2 as
select id_order, id_client, id_product, id_team, id_transport, o_amount, o_date
from orders
where s_date is null
with read only;


//триггеры
//таблица
create table clients_history
(
  id_client number(10),
  l_name varchar(20),
  f_name varchar(20),
  address varchar(20),
  phone varchar(10),
  c_date date,
  c_user varchar(20)
);
_____________
//история клиентов 
create or replace trigger clients_archive
before update or delete  on clients

for each row
declare
begin
insert into clients_history 
values (:old.id_client,:old.l_name,:old.f_name,:old.address,:old.phone,SYSDATE,USER);

end clients_archive;

//контроль цен
create or replace trigger price_control
before insert or update on products
for each row

declare

m varchar(10);
n varchar(10);

error_1 exception;
pragma EXCEPTION_INIT(error_1, -20001); 
error_2 exception;
pragma EXCEPTION_INIT(error_2 , -20002); 


begin
DBMS_OUTPUT.enable;
m:=:new.cost;
n:=:old.cost;

if (m > 5000) then raise error_1;
else if m-n > (n*0.1) then raise error_2;
     end if;
end if;


exception 
when error_1 then RAISE_APPLICATION_ERROR(-20001, 'Zapreshennaya operaciya1');
when error_2 then RAISE_APPLICATION_ERROR(-20002, 'Zapreshennaya operaciya2');
when OTHERS then DBMS_OUTPUT.PUT_LINE('Exception: OTHER');


end price_control;

//подсчёт цены
create or replace trigger price_tr
before insert on orders
for each row

declare

cursor c_cost is
select id_product, cost
from products
where id_product  = :new.id_product;
c_c c_cost%rowtype;


begin

open c_cost;
fetch c_cost into c_c;
close c_cost ;

DBMS_OUTPUT.enable;

:new.price:=c_c.cost*:new.o_amount;


end price_tr;
/////////////////1
create or replace trigger Vip
before insert on Orders
for each row
begin
for item in
(
select  sum(price) prc
from Orders
where id_client =:new.id_client
)
loop
if(item.prc>= 2000)
then 
insert into Clients_vip 
select * from clients
where id_client =:new.id_client;

end if;
end loop;
end Vip;

///////////////создвние пакета
create or replace package my_package as
procedure new_address(last_name varchar, new_address varchar);
function count_products(firm_name varchar) return varchar2;
end;​
///////////////тело пакета
create or replace package body my_package is

procedure new_address(last_name varchar, new_address varchar) as

cursor client_c (l_n clients.l_name%type) is
select id_client
from clients
where (l_name = l_n);

cursor count_c (l_n clients.l_name%type) is
select count(id_client)
from clients
where (l_name = l_n);

kol_clients NUMBER;
i number(10);
n number(10);
oshibka EXCEPTION;

begin
sys.dbms_output.enable;

open client_c(last_name);
fetch client_c into i;
if (client_c%rowcount=0) then dbms_output.put_line('Net takogo sotrydnika!');
else open count_c(last_name);
     fetch count_c into n;
     if (n>1) then raise oshibka;
     else update clients
     set address = new_address
     where id_client = i;
     end if;
     close count_c;
end if;
close client_c;

exception
when oshibka then
RAISE_APPLICATION_ERROR(-20001,'Naideni odnofamilci! Ix kol-vo - ' || n);
when others then
RAISE_APPLICATION_ERROR(-20002,'Oshibka');
end new_address;

function count_products(firm_name varchar)
return varchar2 is out_count varchar2(50);

cursor product_c (f_n firms.f_name%type) is 
select sum(o_amount)
from orders
where id_product in (select id_product
                    from products
                    where id_firm in (select id_firm
                                      from firms
                                      where f_name = f_n));
                                    
cursor firm_c (f_n firms.f_name%type) is 
select id_firm
from firms
where f_name = f_n;
ii number (10);

cursor ip_c is
select id_product, id_firm
from products;
ip ip_c%rowtype;

cursor ip_cc (id_p orders.id_product%type) is
select id_order
from orders
where id_product = id_p and (o_date > add_months(sysdate,-12));
i number(10);

cursor product_cc (id_p products.id_product%type) is
select id_product,id_firm,id_type,p_name,cost,amount
from products
where id_product = id_p;
prod product_cc%rowtype;

begin

open firm_c(firm_name);
fetch firm_c into ii;
close firm_c;

for ip in i
p_c
loop
if (ii = ip.id_firm) then 
    open ip_cc(ip.id_product);
    fetch ip_cc into i;
    if (ip_cc%rowcount = 0) then open product_cc (ip.id_product);k
                                 fetch product_cc into prod;
                                 insert into products_history
                                 values (prod.id_product,prod.id_firm,prod.id_type,prod.p_name,prod.cost,prod.amount);
                                 
                                 delete from products
                                 where id_product = prod.id_product;
                                 
                                 close product_cc;
    end if;
    close ip_cc;
end if;
end loop;

open product_c(firm_name);
fetch product_c into out_count;
close product_c;

if (out_count is null) then out_count:=0;
end if;
return to_char('Prodano za vse vremya - '||out_count);

exception
when others then
RAISE_APPLICATION_ERROR(-20003,'Oshibka');

end count_products;

end my_package;​


/////вызов пакета
declare 
begin
my_package.new_address('Zaxarov', '123');
dbms_output.put_line(my_package.count_products('Philips'));
end;

///////процедура
procedure new_address(last_name varchar, new_address varchar) as

cursor client_c (l_n clients.l_name%type) is
select id_client
from clients
where (l_name = l_n);

cursor count_c (l_n clients.l_name%type) is
select count(id_client)
from clients
where (l_name = l_n);

kol_clients NUMBER;
i number(10);
n number(10);
oshibka EXCEPTION;

begin
sys.dbms_output.enable;

open client_c(last_name);
fetch client_c into i;
if (client_c%rowcount=0) then dbms_output.put_line('Net takogo sotrydnika!');
else open count_c(last_name);
     fetch count_c into n;
     if (n>1) then raise oshibka;
     else update clients
     set address = new_address
     where id_client = i;
     end if;
     close count_c;
end if;
close client_c;

exception
when oshibka then
RAISE_APPLICATION_ERROR(-20001,'Naideni odnofamilci! Ix kol-vo - ' || n);
when others then
RAISE_APPLICATION_ERROR(-20002,'Oshibka');
end new_address;

//вызов процедуры
begin
new_address('Sidorov','Orsha');
end;

//функция
function count_products(firm_name varchar)
return varchar2 is out_count varchar2(50);

cursor product_c (f_n firms.f_name%type) is 
select sum(o_amount)
from orders
where id_product in (select id_product
                    from products
                    where id_firm in (select id_firm
                                      from firms
                                      where f_name = f_n));
                                    
cursor firm_c (f_n firms.f_name%type) is 
select id_firm
from firms
where f_name = f_n;
ii number (10);

cursor ip_c is
select id_product, id_firm
from products;
ip ip_c%rowtype;

cursor ip_cc (id_p orders.id_product%type) is
select id_order
from orders
where id_product = id_p and (o_date > add_months(sysdate,-12));
i number(10);

cursor product_cc (id_p products.id_product%type) is
select id_product,id_firm,id_type,p_name,cost,amount
from products
where id_product = id_p;
prod product_cc%rowtype;

begin

open firm_c(firm_name);
fetch firm_c into ii;
close firm_c;

for ip in ip_c
loop
if (ii = ip.id_firm) then 
    open ip_cc(ip.id_product);
    fetch ip_cc into i;
    if (ip_cc%rowcount = 0) then open product_cc (ip.id_product);
                                 fetch product_cc into prod;
                                 insert into products_history
                                 values (prod.id_product,prod.id_firm,prod.id_type,prod.p_name,prod.cost,prod.amount);
                                 
                                 delete from products
                                 where id_product = prod.id_product;
                                 
                                 close product_cc;
    end if;
    close ip_cc;
end if;
end loop;

open product_c(firm_name);
fetch product_c into out_count;
close product_c;

if (out_count is null) then out_count:=0;
end if;
return to_char('Prodano za vse vremya - '||out_count);

exception
when others then
RAISE_APPLICATION_ERROR(-20003,'Oshibka');

end count_products;


//вызов функции
declare
kol varchar2(50);
begin
sys.dbms_output.enable;
kol:=count_products('Philips');
dbms_output.put_line(kol);
end;



















create or replace trigger Vip
before insert on Orders
for each row
begin
for item in
(
select  sum(price) prc_c
from Orders
where id_client =:new.id_client
--and to_char(o_date,'YYYY')=to_char(sysdate,'YYYY')
and year(o_date)=year(sysdate)
)
for item in
(
select  sum(price) prc_l
from Orders
where id_client =:new.id_client
--and to_char(o_date,'YYYY')=to_char(sysdate,'YYYY')
and year(o_date)=year(add_months(sysdate,-12))
)
loop
if(item.prc_c>= 2000 or item.prc_l>=2000)
then 
insert into Clients_vip 
select * from clients
where clients.id_client =:new.id_client
--;
--
and not exists (select id_client from Clients_vip
              where Clients_vip.id_clients=:new.id_client;
             );
--
end if;
end loop;
end Vip;



///???
create or replace trigger Vip
before insert on orders
for each row
begin
for item_с in
(
select  sum(price) prc_c
from orders
where id_client =:new.id_client
and to_char(o_date,'YYYY')=to_char(sysdate,'YYYY')
--and to_char(o_date,'YYYY')=to_char(add_months(sysdate,-12),'YYYY')
)
loop
if(item_с.prc_c>= 2000)
then 
insert into Clients_vip 
select * from clients
where id_client =:new.id_client;
end if;
end loop;

for item_l in
(
select  sum(price) prc_l
from orders
where id_client =:new.id_client
--and to_char(o_date,'YYYY')=to_char(sysdate,'YYYY')
and to_char(o_date,'YYYY')=to_char(add_months(sysdate,-12),'YYYY')
)
loop
if(item_l.prc_l>= 2000)
then 
insert into Clients_vip 
select * from clients
where id_client =:new.id_client; 
end if;
end loop;

end Vip;



//не работает
create or replace trigger Vip
before insert on orders
for each row
declare
var_count integer;
begin

for item_с in
(
select sum(price) prc_c
from orders
where id_client =:new.id_client
and to_char(o_date,'YYYY')=to_char(sysdate,'YYYY')
--and to_char(o_date,'YYYY')=to_char(add_months(sysdate,-12),'YYYY')
)
select count(id_client) into var_count from clients_vip where id_client = :new.id_client;
loop
if(item_с.prc_c>= 2000 and var_count=0)
then 
insert into Clients_vip 
select * from clients
where id_client =:new.id_client;
end if;
end loop;

for item_l in
(
select sum(price) prc_l
from orders
where id_client =:new.id_client
--and to_char(o_date,'YYYY')=to_char(sysdate,'YYYY')
and to_char(o_date,'YYYY')=to_char(add_months(sysdate,-12),'YYYY')
)
select count(id_client) into var_count from clients_vip where id_client = :new.id_client;
loop
if(item_l.prc_l>= 2000 and var_count=0)
then 
insert into Clients_vip 
select * from clients
where id_client =:new.id_client; 
end if;
end loop;

end Vip;