create or replace
	function searchperson1(fam in varchar2, name in varchar2, otch in varchar2) return  varchar2 is
		sqltext dbms_sql.varchar2s; 
		whereclause dbms_sql.varchar2s; 

	i integer; 
	c integer; 
	b_id number; 
	ex number;
	result varchar2(50);
	text varchar2(200); 
	
begin
	whereclause(1):='true ';
	
	if fam is not null then
		whereclause(whereclause.last+1):=' Фамилия=:xfam';
	end if;
	if name is not null then
		whereclause(whereclause.last+1):=' Имя=:xname';
	end if;

	if otch is not null then
		whereclause(whereclause.last+1):=' Отчество=:xotch';
	end if;
 	
 	sqltext(1):='select код_раб'; 
 	sqltext(2):=' from Рабочие'; 

 	
 	text:=' where'; 
 
 	for i in whereclause.first+1..whereclause.last loop 
 		sqltext(sqltext.last+1):=text; 
 		sqltext(sqltext.last+1):=whereclause(i); 
 		text:=' and'; 
 	end loop; 

	c:=dbms_sql.open_cursor;
		dbms_sql.parse(c, sqltext, sqltext.first, sqltext.last, false, dbms_sql.native);
	if fam is not null then
		dbms_sql.bind_variable(c,':xfam',fam);
	end if;

	if name is not null then
		dbms_sql.bind_variable(c,':xname',name);
	end if;

	if otch is not null then
		dbms_sql.bind_variable(c,':xotch',otch);
	end if;

	dbms_sql.define_column(c,1,b_id);
	ex:=dbms_sql.execute(c);

	loop
		if dbms_sql.fetch_rows(c)>0 then
		dbms_sql.column_value(c,1,b_id);
		result:=b_id;
		return result;
		else
		exit; 
		end if;
	end loop;

	dbms_sql.close_cursor(c);
end;



-------------------------------------------------------------------------------------------------
create or replace procedure
	 searchperson(familyfilter in varchar2, firstnamefilter in varchar2,
	  middlenamefilter in varchar2, result in out dbms_sql.varchar2s) is
		sqltext dbms_sql.varchar2s; /* Текст запроса */


whereclause dbms_sql.varchar2s; /* Часть … where… */
i integer; /* Счетчик */
c integer; /* Идентификатор курсора */
b_id number; /* Буферная переменная для результатов */
begin
	whereclause(1):='true';
	
	if familyfilter is not null then
		whereclause(whereclause.last+1):=' and family like :xfamilyfilter';
	end if;
	
	if firstnamefilter is not null then
		whereclause(whereclause.last+1):=' and firstname like :xfirstnamefilter';
	end if;
	
	if middlenamefilter is not null then
		whereclause(whereclause.last+1):=' and middlename like :xmiddlenamefilter';
	end if;
	/* На этом этапе у нас имеется часть запроса - where, в которой упомянуты только те условия, которые были заданы через непустые параметры хранимой процедуры */
	/* Теперь построим текст запроса полностью */
	
	sqltext(1):='select id';
	sqltext(2):='from personparticulars';
	
	for i in whereclause.first..whereclause.last loop
		sqltext(sqltext.last+1):=whereclause(i);
	end loop;
	
	/* Получаем идентификатор курсора */
	c:=dbms_sql.open_cursor;
	/* Разборка текста запроса */
	dbms_sql.parse(c, sqltext, sqltext.first, sqltext.last, false, dbms_sql.native);
	/* Установка параметров запроса */
	if familyfilter is not null then
		dbms_sql.bind_variable(c,':xfamilyfilter',familyfilter);
	end if;
	
	if firstnamefilter is not null then
		dbms_sql.bind_variable(c,':xfirstnamefilter',firstnamefilter);
	end if;
	
	if middlenamefilter is not null then
		dbms_sql.bind_variable(c,':xmiddlenamefilter',middlenamefilter);
	end if;
	
	/* Установка столбцов в запросе */
	dbms_sql.define_column(c,1,b_id);
	/* Выполнение запроса */
	dbms_sql.execute(c);
	/* Выборка результатов запроса */
	loop
		/* Выбираем следующую строку */
		if dbms_sql.fetch_rows(c)>0 then
			dbms_sql.column_value(c,1,b_id);
			/* В этот момент в переменной b_id имеем текущее значение id очередной строки. Что с ней делать, уже дело разработчика */
			else
			exit; /* Если нет больше строк, вываливаемся */
		end if;
	end loop;
	/* Закрываем курсор */
	dbms_sql.close_cursor(c);
end;