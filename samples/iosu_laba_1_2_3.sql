﻿CREATE TABLE branch_tr(
bno INTEGER not null,
PRIMARY KEY (bno),
street VARCHAR2(30) not null,
city VARCHAR2(30) not null,
tel_no INTEGER UNIQUE,
CHECK (city IN ('Vitebsk','Brest','Grodno'))
);

CREATE TABLE staff_tr(
sno INTEGER not null,
PRIMARY KEY(sno),
fname VARCHAR2(30) not null,
lname VARCHAR2(30) not null,
addres VARCHAR2(30) not null,
tel_no INTEGER not null,
position VARCHAR(30) not null,
sex VARCHAR(6) not null,
dob DATE not null,
salary INTEGER not null,
bno INTEGER not null,
CONSTRAINT ss1 FOREIGN KEY (bno) REFERENCES branch_tr,
CONSTRAINT ss2 CHECK(sex = 'male' or sex = 'female')
);


CREATE TABLE property_for_rent_tr(
 pno INTEGER not null,
 PRIMARY KEY(pno),
 street VARCHAR2(30) not null,
 city VARCHAR2(30) not null,
 type CHAR not null,
 rooms INTEGER not null,
 rent INTEGER not null,
 ono INTEGER not null,
 sno INTEGER not null,
 bno INTEGER not null,
 CONSTRAINT ss3 FOREIGN KEY (ono) REFERENCES owner_tr,
 CONSTRAINT ss4 FOREIGN KEY (sno) REFERENCES staff_tr,
 CONSTRAINT ss5 FOREIGN KEY (bno) REFERENCES branch_tr,
 CONSTRAINT ss6 CHECK(type = 'h' or type = 'f')
);

CREATE TABLE renter_tr(
 rno INTEGER not null,
 fname VARCHAR2(30) not null,
 lname VARCHAR2(30) not null,
 address VARCHAR2(30) not null,
 tel_no INTEGER not null,
 pref_type CHAR not null,
 max_rent INTEGER not null,
 bno INTEGER not null,
 PRIMARY KEY (rno),
 CONSTRAINT a1 FOREIGN KEY (bno) REFERENCES branch_tr,
 CONSTRAINT a2 CHECK(pref_type = 'h' or pref_type = 'f')
);

 CREATE TABLE owner_tr (
 ono INTEGER not null,
 fname VARCHAR2(30) not null,
 lname VARCHAR2(30) not null,
 address VARCHAR2(30) not null,
 tel_no INTEGER not null,
 PRIMARY KEY(ono)
); 


CREATE TABLE viewing_tr(
 rno INTEGER not null,
 pno INTEGER not null,
 date1 DATE not null,
 comment1 VARCHAR2(80),
 CONSTRAINT b1 PRIMARY KEY (rno, pno),
 CONSTRAINT b2 FOREIGN KEY (rno) REFERENCES renter_tr,
 CONSTRAINT b3 FOREIGN KEY (pno) REFERENCES property_for_rent_tr
);
 
INSERT INTO branch_tr VALUES (1,'Levanevskogo', 'Brest', 4355678);
INSERT INTO branch_tr VALUES (2,'Kirova', 'Grodno', 4355458);
INSERT INTO branch_tr VALUES (3,'Vasilka', 'Grodno', 4444678);
INSERT INTO branch_tr VALUES (4,'Putevaya', 'Brest', 4354788);
INSERT INTO branch_tr VALUES (5,'Titova', 'Vitebsk', 4234678);
INSERT INTO branch_tr VALUES (6,'Smolenskaya', 'Vitebsk', 7356478);
 
INSERT INTO staff_tr VALUES (1, 'Misha', 'Mironov', 'Kievskaya', 2348759, 'first', 'male', '09.11.1978', 900, 5);
INSERT INTO staff_tr VALUES (2, 'Fedya', 'Alekseev', 'Karla Marksa', 2348759, 'second', 'male', '09.11.1978', 900, 6);
INSERT INTO staff_tr VALUES (4, 'Vova', 'Suxanov', 'Lelyvelya', 2456759, 'first', 'male', '10.08.1990', 800, 3);
INSERT INTO staff_tr VALUES (6, 'Sasha', 'Bolshakov', 'Avtomobilistov', 9743459, 'first', 'male', '10.08.1990', 1200, 3);
INSERT INTO staff_tr VALUES (7, 'Lena', 'Guseva', 'Avtomobilistov', 9564359, 'second', 'female', '10.08.1990', 1000, 4);

INSERT INTO renter_tr VALUES (1, 'Petya', 'Komarov', 'Kosmonavtov', 4445566, 'h', 500, 6);
INSERT INTO renter_tr VALUES (2, 'Natasha', 'Andreeva', 'Beregovaya', 5554466, 'f', 700, 5);
INSERT INTO renter_tr VALUES (3, 'Andrey', 'Kudryavqev', 'Volinskaya', 4568761, 'f', 700, 4);
INSERT INTO renter_tr VALUES (4, 'Galya', 'Gorshkova', 'Volinskaya', 4846561, 'h', 650, 3);
INSERT INTO renter_tr VALUES (5, 'Vasya', 'Ermolaev', 'Boldina', 9986754, 'f', 650, 2);
INSERT INTO renter_tr VALUES (6, 'Olya', 'Nesterova', 'Gribnaya', 5555567, 'h', 800, 1);

INSERT INTO owner_tr VALUES (1, 'Pasha', 'Lebedev', 'Brest ul.Gogolya', 1234765);
INSERT INTO owner_tr VALUES (2, 'Lesha', 'Lazarev', 'Brest ul.Zavetnaya', 1333465);
INSERT INTO owner_tr VALUES (3, 'Dasha', 'Simonava', 'Grodno ul.Dombrovskogo', 1555585);
INSERT INTO owner_tr VALUES (4, 'Masha', 'Romanova', 'Grodno ul.Zapadnaya', 7676764);
INSERT INTO owner_tr VALUES (5, 'Stepa', 'Melnikov', 'Vitebsk ul.Kalinina', 5454345);
INSERT INTO owner_tr VALUES (6, 'Sergey', 'Ponomarev', 'Vitebsk ul.Olxovaya', 8762340);

INSERT INTO property_for_rent_tr VALUES (1, 'Klenovaya', 'Brest', 'h', 2, 650, 1, 7, 4);
INSERT INTO property_for_rent_tr VALUES (2, 'Kuybisheva', 'Brest', 'f', 4, 1000, 1, 7, 4);
INSERT INTO property_for_rent_tr VALUES (3, 'Elskogo', 'Grodno', 'h', 1, 350, 4, 4, 3);
INSERT INTO property_for_rent_tr VALUES (4, 'Novaya', 'Grodno', 'f', 3, 650, 3, 6, 3);
INSERT INTO property_for_rent_tr VALUES (5, 'Pionerskaya', 'Vitebsk', 'h', 3, 650, 5, 1, 5);
INSERT INTO property_for_rent_tr VALUES (6, 'Sportivnaya', 'Vitebsk', 'h', 5, 1200, 6, 2, 6);

INSERT INTO viewing_tr VALUES (1, 2, '12.08.2014', 'xata ogon');
INSERT INTO viewing_tr VALUES (1, 1, '12.08.2014', '');
INSERT INTO viewing_tr VALUES (2, 1, '12.08.2014', '');
INSERT INTO viewing_tr VALUES (2, 4, '12.08.2014', '');

2 laba
//1 zapros
SELECT * FROM staff_tr
WHERE salary BETWEEN 200 AND 300;

//2 zapros
SELECT fname, lname, S_tr.tel_no
FROM branch_tr B_tr, staff_tr S_tr
WHERE B_tr.bno=S_tr.bno and city IN ('Brest', 'Minsk');

//3 zapros
 SELECT position, AVG(salary) AS average_salary, SUM(salary) AS total_salary
 FROM staff_tr
 GROUP BY position;

//4 zapros
SELECT position,salary, lname, fname
FROM staff_tr
WHERE salary > (SELECT AVG(salary) FROM staff_tr);


//3 laba
/1.1
CREATE OR REPLACE VIEW ofices_brest AS
SELECT * FROM branch_tr 
WHERE city='Brest';
/1.2 жилье где стоимость в пределах 20% от минимальной
CREATE OR REPLACE VIEW houseroom_with_min_cost AS
SELECT * FROM property_for_rent_tr
WHERE rent BETWEEN (select MIN(rent) FROM property_for_rent_tr) AND ((select MIN(rent) FROM property_for_rent_tr)*1.2);
/1.3
CREATE OR REPLACE VIEW views_with_comments AS
SELECT * FROM viewing_tr
WHERE comment1 IS NULL;

CREATE OR REPLACE VIEW views_with_comments AS
SELECT * FROM viewing_tr
WHERE comment1 IS NOT NULL;
/1.4
трехкомнатные квартиры?
CREATE OR REPLACE VIEW f_3_apartment AS
SELECT * FROM property_for_rent_tr
WHERE type='f' AND rooms=3;

/1.5
CREATE OR REPLACE VIEW max_staff_tr AS
SELECT *
FROM branch_tr
WHERE bno in
		(
		SELECT bno
		FROM staff_tr GROUP BY bno
			HAVING COUNT(bno)
			IN 
		(
		SELECT MAX(COUNT(sno))
		FROM staff_tr GROUP BY bno
		)
		)	
		;


/1.6
CREATE OR REPLACE VIEW inf AS
SELECT * FROM staff_tr JOIN property_for_rent_tr ON staff_tr.sno=property_for_rent_tr.sno;

CREATE OR REPLACE VIEW inf AS
SELECT fname, lname, tel_no, position, sex, dob, salary, 
street, city, type, rooms, rent FROM staff_tr JOIN property_for_rent_tr ON staff_tr.sno=property_for_rent_tr.sno;

CREATE OR REPLACE VIEW inf AS
SELECT fname, lname, tel_no, position, sex, dob, salary, 
street, city, type, rooms, rent FROM staff_tr ss JOIN property_for_rent_tr pp ON ss.sno=pp.sno;
/1.7
CREATE OR REPLACE VIEW check_house AS
SELECT * FROM owner_tr
WHERE ono IN (SELECT ono FROM property_for_rent_tr p, viewing_tr v WHERE v.pno = p.pno);










SQL> SET LINESIZE 9999
SQL> SET PAGESIZWn 100
SP2-0158: íåèçâåñòíàÿ SET îïöèÿ "PAGESIZWn"
SQL> SET PAGESIZE 100
SQL> select * from max_staff_tr;

       BNO STREET                         CITY                               TEL_NO
---------- ------------------------------ ------------------------------ ----------
         3 Vasilka                        Grodno                            4444678