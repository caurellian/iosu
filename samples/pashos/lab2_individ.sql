--������ �������,����������� � ������ ������(��������)
select g_name,sec_name 
	from tovary 
	where sec_name='Meat';

--������ ������ �� ������� (��������)(inner join)
select g.sec_name as "Section ", sum(s.sa_value) as "items sold"
	from goods g inner join sales s on g.g_name = s.g_name 
	group by g.sec_name;
	
--����� ������ ������� � ���������(�����������)
select sec_name as "sections and sellers"from sections 
	union
	select se_fname || ' ' || se_lname || ' ' || se_mname 
	from sellers;
	
--������ ��������� � �������� ������(��������)
select g_name as "Product in section" from  goods where sec_name='&Section_name';

--����� ������ �� ������� ���� (����)
select sum(g.g_price) as "SUM", to_char(sa.sa_date,'ww') as "YW" 
	from sales sa inner join goods g on sa.g_name=g.g_name 
	group by to_char(sa.sa_date,'ww');

--������, ��������� ����� ����� n
select g_name,sa_value
	from sales 
	where(sa_check = &check_number);
	
--������ � �������� ������ ��������
select * 
	from goods 
	where (g_doexpiriation <= sysdate);
	
--������ � ����� �������� ������ ��������(5 ����)
select * 
	from goods 
	where (g_doexpiriation between sysdate and sysdate+5);
	
--����� � ����������� ������������ ������� ������ n (� ���������� ������� ���������� ������������ �������)
select sec_name, max(sum_exp_value) 
	from 
	(select sec_name, sum(g_whvalue) as sum_exp_value 
		from goods 
		where (g_doexpiriation<=sysdate) group by sec_name)
	group by sec_name having (max(sum_exp_value)>&n_expired_products);

--������, ������������� �� ������
select g_name 
	from goods 
	where (g_whvalue = 0 or g_whvalue is null);
	
--������ � ������! ����������� ���� ������� �� n ���������!
update goods
	set g_price=g_price*(1+&n_percent/100);
	
--������� ��������� (���������� ��������� ������� � �����������)
select se.se_fname,se.se_lname,se.se_mname, sum(sa.sa_value) as total_sales
	from sellers se inner join sales sa on se.se_servrec_no=sa.se_servrec_no 
	group by (se.se_fname,se.se_lname,se.se_mname) 
	order by (sum(sa.sa_value)) desc;
	
--������, �� ������������ ������� ����� n ���� (������� ������, ��� ��������� ������ ����� ���� �� �������) (full join)
select g.g_name,sa.sa_date 
	from goods g full join sales sa on sa.g_name=g.g_name 
	where (sa.sa_date < sysdate-&n_days or sa.sa_date is null);


--������� ����������, � �������� ��������� ������ ������ ��� ����� ������� ���� ������ �� ����������� (all)
select g.d_name
	from goods g
	where g.g_price > all (select avg(g_price)
						from goods 
						group by d_name);
						
--������� ������ ����������, ������� ���������� ��������� ��� ����� �������� (exist)
select *
	from distributors d
	where exists (select * 
					from goods g
					where g.g_doexpiriation is null and d.d_name = g.d_name);
					
-- ������� ������ ����������, � �������� �������� ��������� (not exists)
select *
	from distributors d
	where not exists (select * 
					from goods g
					where g.g_doexpiriation is null and d.d_name = g.d_name);
--������� ����������, � �������� ���� ���� �� �� ���� ������� ������ ��� ������� �� ����������� (Any)
select d_name
	from goods
	where g_price < any (select avg(g_price) 
						from goods 
						group by d_name)
	group by d_name;
	
	----
	--������� ������ ����������, � �������� ���� ���� �� �� ���� ������� ������ ��� price_value  (Any)
--select *
	--from distributors
	--where d_name = any (select d_name 
		--				from goods 
			--			where g_price<&price_value);

			---------======================������===================--------------
		--���������� ��������� ������� �� �����
		select * 
			from 
			(
				select count(*) cnt,g_name product,sa_value val, to_char(sa_date,'yyyy') yr 
				from sales 
				group by g_name, to_char(sa_date,'yyyy'), sa_value
			) 
			pivot 
			(
				sum(cnt*val) 
				for yr 
				in('2012','2013','2014','2015') 
			);
			
			
			-------------------------------
select distinct d_name
	from goods a
	where g_price < any (select avg(g_price) 
						from goods b
						 where a.d_name = b.d_name
						 group by d_name)
	;
	