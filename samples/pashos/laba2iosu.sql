	
	---------------------laba 2--------------------
	--список менеджеров женщин
	select fname,lname from staff where sex='f' and position like 'Mn%';
	
	--максимальная зарплата сотрудников в гродно
	select max(salary) as "Max salary from Grodno" 
		from staff 
		where bno in (select bno from branch where city='Grodno');
	
	--количество осмотров объектов в день
	select (count(date1)/(sysdate-min(date1))) as "Viewing per day" from viewing;
	
	--вывести инф об отделении, где предлагаются в аренду 2х комнатные кавртиры с максимальной средней стоимостью(Ужас!)
		select * from branch where bno in   --вывести все об отделении с номером
		(
			select bno   -- номер отдела, выбирающися из                
			from 					
			(
				select bno,avg(rent) as avg_rent           --отдел и средняя стоимость по двухкомнатным квартирам, сгруппированные по отделам 
				from property_for_rent where rooms=2 group by bno
			) 
			where avg_rent=                      --где средняя стоимость
			(
				select max(avg_rent)			--равна максимальной 
				from 							 --из всех возможных средних	
				(
					select bno,avg(rent) as avg_rent 
					from property_for_rent where rooms=2 group by bno
				)
			)
		);
		
	--то же самое, только через представление
	
	create view avg_2room_rent as 		-- создаем представление, которое выводит отделение со средней стоимостью 2хкомнатных квартир (bno, avg_rent)
		select bno,avg(rent) as avg_rent           
		from property_for_rent 
		where rooms=2 group by bno;
		
	select * --выводит все об отделении, в котором максимальная средняя рентная
		from branch 
		where bno in (select bno 
						from avg_2room_rent 
						where avg_rent=(select max(avg_rent) 
										from avg_2room_rent)); 
		
	