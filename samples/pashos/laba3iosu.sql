------------------laba 3--------------------------------
	--1 	с информацией об офисах в Бресте;
	create or replace view branch_stuff_info as	
		select * 
		from branch
		where city='Brest';	
	--2 	с информацией об объектах недвижимости минимальной стоимости;
	create or replace view minimum_pfr_rent as
		select *
		from property_for_rent
		where rent = (select (min(rent)) from property_for_rent);
	--3	с информацией о количестве сделанных осмотров с комментариями;
	create or replace view count_views_with_comment as
		select (count(comment1)) as viewving_with_comments
		from viewing
		where comment1 is not null;
	--4	со сведениями об арендаторах, желающих арендовать 3-х комнатные квартиры в тех же городах, где они проживают;
	create or replace view renter_wish_f_3 as
		select r.* 
		from renter r
		where (select pfr.type
					from property_for_rent pfr 
					where pfr.rooms=3 and (r.adress like '%'||pfr.city||'%'))= 'f';
	--5	со сведениями об отделении с максимальным количеством работающих сотрудников;
	create or replace view branch_w_max_stuff
	select * 
		from branch
		where bno in(
			select bno 
			from staff
			group by bno
			having count(bno) in 
				(select max(count(sno))
				from staff
				group by bno));
				
	--6	с информацией о сотрудниках и объектах, которые они предлагают в аренду в текущем квартале;
	create or replace view info as
		select fname, lname, tel_no, city, type, rooms, rent 
		from staff s 
		join property_for_rent pfr 
		on s.sno=pfr.sno
		where pno in
			(select pno 
				from viewing
				where to_char(date1,'Q')=to_char(sysdate,'Q'));
	
	--7 	с информацией о владельцах, чьи дома или квартиры осматривались потенциальными арендаторами более двух раз.
	create or replace view check_house_more_than2 as
		select * 
		from owner	
			where ono in
				(select ono 
				from property_for_rent
				where pno in	
					(select pno
					from viewing
					group by pno
					having count(rno)>2));
					
	
